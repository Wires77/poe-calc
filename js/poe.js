var aurasEncode = [
  [
    // add new things up here
    ['ASPECT', 3],
    ['REDUCED_MANA', 7],
    ['BLASPHEMY', 3],
    ['ENLIGHTEN', 4],
    ['BLOOD_GEM', 5],
    ['CLARITY', 5],
    ['MULTIPLIER', 10]
  ],
  [
    // add new things up here
    ['AULS_UPRISING', 1],
    ['HERALD_AGONY', 1],
    ['HERALD_PURITY', 1],
    ['PURITY_ELEMENTS', 1],
    ['THE_COVENANT', 1],
    ['ENVY', 1],
    ['HERETICS_VEIL', 1],
    ['ESSENCE_WORM', 1],
    ['ENHANCE', 1],
    ['VICTARIOS', 1],
    ['GENEROSITY', 1],
    ['EMPOWER', 1],
    ['PRISM_GUARDIAN', 1],
    ['WRATH', 1],
    ['VITALITY', 1],
    ['ARCTIC', 1],
    ['PURITY_LIGHTNING', 1],
    ['PURITY_ICE', 1],
    ['PURITY_FIRE', 1],
    ['PURITY_ELEMENTS', 1],
    ['HERALD_THUNDER', 1],
    ['HERALD_ICE', 1],
    ['HERALD_ASH', 1],
    ['HATRED', 1],
    ['HASTE', 1],
    ['GRACE', 1],
    ['DISCIPLINE', 1],
    ['DETERMINATION', 1],
    ['ANGER', 1]
  ]
]

var settingsEncode = [
  // add new things up here
  ['SAQAWALS_NEST', 1],
  ['MASTERMIND_DISCORD', 1],
  ['MEMORY_VAULT', 1],
  ['IMPRESENCE', 1],
  ['CALAMITY', 1],
  ['SANCTUARY_OF_THOUGHT', 1],
  ['PERFECT_FORM', 1],
  ['CONQUERORS', 1],
  ['ICHIMONJI2', 1],
  ['ICHIMONJI', 1],
  ['MIDNIGHT_BARGAIN2', 1],
  ['MIDNIGHT_BARGAIN', 1],
  ['ALPHAS_HOWL', 1],
  ['MORTAL_CONVICTION', 1],
  ['BLOOD_MAGIC', 1],
  ['SKYFORTH', 1],
  ['reducedMana', 6],
  ['mana', 15],
  ['life', 15]
]

var alpha = {
  // changing this index will break all previous links, so don't be a jerk
  index: 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ=_-',

  encode: function(encnum) {

    var ret = ''

    for (var i = Math.floor(Math.log(parseInt(encnum)) / Math.log(alpha.index.length)); i >= 0; i--) {
      ret = ret + alpha.index.substr((Math.floor(parseInt(encnum) / alpha.bcpow(alpha.index.length, i)) % alpha.index.length), 1)
    }

    return ret.reverse()
  },

  decode: function(decstr) {
    var str = decstr.reverse()
    var ret = 0

    for (var i = 0; i <= (str.length - 1); i++) {
      ret = ret + alpha.index.indexOf(str.substr(i, 1)) * (alpha.bcpow(alpha.index.length, (str.length - 1) - i))
    }

    return ret
  },

  bcpow: function(_a, _b) {
    return Math.floor(Math.pow(parseFloat(_a), parseInt(_b)))
  }
}
var pad = function(num, amount) {
  var zeros = new Array(amount + 1).join("0")
  return (zeros + "" + num).slice(amount * -1)
}
String.prototype.reverse = function() {
  return this.split("").reverse().join('')
}

var globalTmp = {}

/* http://www.pathofexile.com/forum/view-thread/567561/page/3 */
var calculateAura = function(aura, reducedMana = 0, lessMana = [], multiplier = 100, isFlat = false) {
  var base = Math.floor(aura * (multiplier / 100))
  var reducedPercentage = ((100 - reducedMana) / 100)
  var lessPercentage = lessMana.reduce((x, v) => (x * ((100 - v) / 100)), 1)
  var total = reducedPercentage < 1 ? Math.ceil(base * reducedPercentage) : Math.floor(base * reducedPercentage)
  return Math.max(Math.floor(total * lessPercentage), 0)
}

var heraldCalc = function(pl) {
  if(pl.rootScope.settings['CALAMITY']) {
    return 45
  }

  var localReducedMana = pl.localReducedMana

  if (pl.rootScope.settings['MASTERMIND_DISCORD'] == true) {
    localReducedMana += pl.rootScope.ITEMS[0]['MASTERMIND_DISCORD'].reduced
  }

  return calculateAura(pl.rootScope.AURAS[pl.aura].cost, localReducedMana, pl.globalLessMana, pl.localMutliplier)
}

var blasphemyCalc = function(pl) {
  var localReducedMana = pl.localReducedMana
  if(pl.rootScope.itemGroup['HERETICS_VEIL'][pl.auraGroup]) {
   localReducedMana += pl.rootScope.LOCAL_ITEMS['HERETICS_VEIL'].reduced
  }

  var number = parseInt(pl.rootScope.auraGroup[pl.aura][pl.auraGroup]) || 0

  // Impresence makes first aura in the calc free
  if(pl.rootScope.settings['IMPRESENCE'] == true && number > 0 && !globalTmp['IMPRESENCE_USED']) {
    globalTmp['IMPRESENCE_USED'] = true
    return calculateAura(pl.rootScope.AURAS[pl.aura].cost, localReducedMana + 100, pl.globalLessMana, pl.localMutliplier)
    + (calculateAura(pl.rootScope.AURAS[pl.aura].cost, localReducedMana, pl.globalLessMana, pl.localMutliplier) * (number - 1))
  }
  else {
    return calculateAura(pl.rootScope.AURAS[pl.aura].cost, localReducedMana, pl.globalLessMana, pl.localMutliplier) * number
  }
}

var arcticCalc = function(pl) {
  var localReducedMana = pl.localReducedMana
  if(pl.rootScope.settings['PERFECT_FORM']) {
    localReducedMana += pl.rootScope.ITEMS[1]['PERFECT_FORM'].reduced
  }
  return calculateAura(pl.rootScope.AURAS[pl.aura].cost, localReducedMana, pl.globalLessMana, pl.localMutliplier)
}

var wrathCalc = function(pl) {
  if(pl.rootScope.settings['AULS_UPRISING']) {
    return 0
  }
  return calculateAura(pl.rootScope.AURAS[pl.aura].cost, pl.localReducedMana, pl.globalLessMana, pl.localMutliplier)
}

angular.module("poeAura", [])
.constant("AURAS", {
    ANGER: { cost: 50, aura: true, title: "Anger" },
    ARCTIC: { cost: 25, buff: true, title: "Arctic Armour", override: arcticCalc },
    ASPECT: { cost: 25, buff: true, title: "Aspect", singleImg: true, max: 4, number: true, description: "There are currently 4 aspect auras introduced in bestiary: cat, avian, spider, and crab" },
    BLASPHEMY: { cost: 35, title: "Blasphemy", singleImg: true, max: 7, number: true, override: blasphemyCalc, description: "The ingame curse limit is 6, each aura stacked has a mana reservation override of 35%" },
    CLARITY: { flat: [0, 34, 48, 61, 76, 89, 102, 115, 129, 141, 154, 166, 178, 190, 203, 214, 227, 239, 251, 265, 279, 293, 303, 313, 323, 333, 343, 353, 363, 373, 383, 383], title: "Clarity", aura: true, number: true, max: 30 },
    DETERMINATION: { cost: 50, aura: true, title: "Determination" },
    DISCIPLINE: { cost: 35, aura: true, title: "Discipline" },
    ENVY: { cost: 50, aura: true, item: true, title: "Envy", description: "Granted by United in Dream Cutlass" },
    GRACE: { cost: 50, aura: true, title: "Grace" },
    HASTE: { cost: 50, aura: true, title: "Haste" },
    HATRED: { cost: 50, aura: true, title: "Hatred" },
    HERALD_AGONY: { cost: 25, buff: true, title: "Herald of Agony", override: heraldCalc },
    HERALD_ASH: { cost: 25, buff: true, title: "Herald of Ash", override: heraldCalc },
    HERALD_ICE: { cost: 25, buff: true, title: "Herald of Ice", override: heraldCalc },
    HERALD_PURITY: { cost: 25, buff: true, title: "Herald of Purity", override: heraldCalc },
    HERALD_THUNDER: { cost: 25, buff: true, title: "Herald of Thunder", override: heraldCalc },
    PURITY_FIRE: { cost: 35, aura: true, title: "Purity of Fire" },
    PURITY_ELEMENTS: { cost: 35, aura: true, title: "Purity of Elements" },
    PURITY_ICE: { cost: 35, aura: true, title: "Purity of Ice" },
    PURITY_LIGHTNING: { cost: 35, aura: true, title: "Purity of Lightning" },
    VITALITY: { cost: 35, aura: true, title: "Vitality" },
    WRATH: { cost: 50, aura: true, title: "Wrath", override: wrathCalc }
})
.constant("LOCAL_ITEMS", {
    BLOOD_GEM: { multi: [100, 245, 242, 239, 237, 234, 232, 229, 226, 224, 221, 218, 216, 213, 211, 208, 205, 203, 200, 197, 196, 193, 190, 187, 184, 181, 178, 175, 172, 169, 166], number: true, max: 30, title: "Blood Magic Gem", bloodMagic: true },
    ENLIGHTEN: { multi: [100, 100, 96, 92, 88, 84, 80, 76, 72, 68, 64], number: true, max: 10, title: "Enlighten" },
    EMPOWER: { multi: 125, title: "Empower" },
    ENHANCE: { multi: 115, title: "Enhance" },
    GENEROSITY: { multi: 100, title: "Generosity" },
    AULS_UPRISING: { reduced: 100, title: "Aul's Uprising", special: true, description: "Makes random aura cost no mana. For purposes of this calculator, it makes everything in the aura group reserve nothing, and you can only select one aura (the amulet doesn't have a socket - so the aura gem would be elsewhere)" },
    ESSENCE_WORM: { multi: 0, type: "RING", title: "Essence Worm", special: true, description: "Socketed aura will have no mana reservation cost, but increases global mana reservation costs by 40% for each ring" },
    HERETICS_VEIL: { reduced: 12, type: "HELM", title: "Heretic's Veil", special: true, description: "Reduced mana multiplier only applies to Blasphemy curses" },
    PRISM_GUARDIAN: { reduced: 25, type: "SHIELD", title: "Prism Guardian", bloodMagic: true },
    THE_COVENANT: { reduced: 0, type: "CHEST", title: "The Covenant", bloodMagic: true },
    VICTARIOS: { reduced: 30, type: "CHEST", title: "Victario's Influence" }
})
.constant("ITEMS", [{
    BLOOD_MAGIC: { multi: 100, title: "Blood Magic", bloodMagic: true },
    MORTAL_CONVICTION: { less: 50, disabled: "!settings['BLOOD_MAGIC']", title: "Mortal Conviction", bloodMagic: true },
    MASTERMIND_DISCORD: { reduced: 25, title: "Mastermind of Discord", special: true, description: "From the Elementalist Ascendancy, only applies to heralds" },
    SANCTUARY_OF_THOUGHT: { less: 10, title: "Sanctuary of Thought", description: "From the Hierophant Ascendancy" }
  }, {
    ALPHAS_HOWL: { reduced: 8, type: "HELM", title: "Alpha's Howl" },
    CONQUERORS: { reduced: 2, title: "Conqueror's Efficiency" },
    IMPRESENCE: { reduced: 100, type: "AMULET", special: true, title: "Impresence", description: "For the purposes of this calculator, it will set your first blasphemy aura to have 100% reduced reservation" },
    MEMORY_VAULT: { reduced: -10, type: "HELM", title: "Memory Vault" },
    SKYFORTH: { reduced: 6, type: "BOOTS", title: "Skyforth" },
    SAQAWALS_NEST: { reduced: 10, type: "CHEST", title: "Saqawal's Nest", description: "Item has reduced mana roll range of 6 to 10, but for the purposes of this calculator I assume you got a maxed roll" },
    CALAMITY: { setTo: 45, type: "CHEST", title: "The Coming Calamity", special: true, description: "All hearlds are always set to 45% reservation" },
    PERFECT_FORM: { reduced: 100, type: "CHEST", title: "The Perfect Form", special: true, description: "Gives arctic armour 100% reduced reservation" }
  }, {
    ICHIMONJI: { reduced: 5, type: "1HAND", title: "Ichimonji" },
    ICHIMONJI2: { reduced: 5, type: "1HAND", title: "Ichimonji x2", disabled: "!settings['ICHIMONJI']"  },
  }, {
    MIDNIGHT_BARGAIN: { setTo: 30, type: "WAND", title: "Midnight Bargain", bloodMagic: true },
    MIDNIGHT_BARGAIN2: { setTo: 30, type: "WAND", title: "Midnight Bargain x2", bloodMagic: true, disabled: "!settings['MIDNIGHT_BARGAIN']" },
  }
])
.run(function ($rootScope, AURAS, LOCAL_ITEMS, ITEMS) {
    $rootScope.AURAS = AURAS
    $rootScope.LOCAL_ITEMS = LOCAL_ITEMS
    $rootScope.ITEMS = ITEMS
    $rootScope.ALL_ITEMS = {}
    $rootScope.viewing = false
    $rootScope.ALL_ITEMS = ITEMS.reduce(function(result, current) {
      return Object.assign(result, current)
    }, {})
    $rootScope.Math = window.Math

    $rootScope.resetForm = function(skipPrompt) {
      if(!skipPrompt && !confirm('Are you sure you want to reset the page?')) {
        return
      }
      $rootScope.reserved = {}
      $rootScope.multiplier = { BLOOD_GEM: {}, ENLIGHTEN: {} }
      $rootScope.auraGroups = [0]
      $rootScope.auraGroup = { REDUCED_MANA: { 0: 0 }, MULTIPLIER: { 0: 100 } }
      $rootScope.bmGroup = {}
      $rootScope.itemGroup = {}
      $rootScope.settings = { reducedMana: 0 }
      $rootScope.viewing = false
      $rootScope.life = $rootScope.mana = 1000
      $rootScope.onlyNumbers = /^\d+$/
      $rootScope.lifeReservedPercent = $rootScope.manaReservedPercent = 0


      Object.keys(AURAS).forEach(function(elem) {
        $rootScope.reserved[elem] = {}
        $rootScope.auraGroup[elem] = {}
      })

      Object.keys(LOCAL_ITEMS).forEach(function(elem) {
        $rootScope.itemGroup[elem] = {}
      })

      for (let item in ITEMS) {
        Object.keys(ITEMS[item]).forEach(function(elem) {
          $rootScope.settings[elem] = false
        })
      }

      if(!skipPrompt) {
        $rootScope.recalc()
        location.replace("#")
      }
    }
    $rootScope.resetForm(true)

    $rootScope.addAuraGroup = function() {
      let lastElement = $rootScope.auraGroups[$rootScope.auraGroups.length-1]
      $rootScope.auraGroups.push(lastElement + 1)
      $rootScope.auraGroup.MULTIPLIER[lastElement + 1] = 100
      $rootScope.auraGroup.REDUCED_MANA[lastElement + 1] = 0
      $rootScope.recalc()
    }

    $rootScope.isDimmed = function(bm, flat, key, index) {
      var reserved = $rootScope.reserved[key][index]
      var isFlat = typeof flat != "undefined"
      if($rootScope.reserved['NEXT_' + key] && $rootScope.reserved['NEXT_' + key][index]) {
        reserved = $rootScope.reserved['NEXT_' + key][index]
      }
      if(isFlat) {
        return (bm && $rootScope.lifeReservedNumeric + reserved) >= $rootScope.life ||
          (!bm && $rootScope.manaReservedNumeric + reserved) > $rootScope.mana
      }
      else {
        return (bm && $rootScope.lifeReservedPercent + reserved) >= 100 ||
          (!bm && $rootScope.manaReservedPercent + reserved) > 100
      }
    }

    $rootScope.deleteAuraGroup = function(index, skipPrompt) {
      if(!skipPrompt && !confirm(`Are you sure you wish to delete aura group ${index + 1}?`)) {
        return
      }

      // Compress array as necessary
      for(let i=index; i < $rootScope.auraGroups.length; i++) {
        for (let localItem in LOCAL_ITEMS) {
          if(i == $rootScope.auraGroups.length) {
            delete $rootScope.itemGroup[localItem][localItem][i]
          }
          else {
            $rootScope.itemGroup[localItem][i] = $rootScope.itemGroup[localItem][i + 1]
          }
        }
        for (let aura in AURAS) {
          if(i == $rootScope.auraGroups.length) {
            delete $rootScope.auraGroup[aura][i]
          }
          else {
            $rootScope.auraGroup[aura][i] = $rootScope.auraGroup[aura][i + 1]
          }
        }
      }

      // Remove aura group
      var auraGroups = []
      for(let i=0; i < $rootScope.auraGroups.length - 1; i++) {
        auraGroups.push(i)
      }
      $rootScope.auraGroups = auraGroups
      $rootScope.recalc()
    }

    // Disables items around special cases like essence worm or aul's uprising
    $rootScope.specialDisabled = function(name, index) {
      // Disable everything while viewing, or number fields
      if($rootScope.viewing) {
        return true
      }

      // Essence worm selected
      if($rootScope.itemGroup['ESSENCE_WORM'][index]) {
        return name !== 'ESSENCE_WORM'
      }


      var aurasSelected = 0

      if(name == 'ESSENCE_WORM' || name == 'AULS_UPRISING') {
        for (let aura in AURAS) {
          if($rootScope.auraGroup[aura][index]) {
            // Aura is incompatible
            if(AURAS[aura].number && !AURAS[aura].flat) {
              return true
            }
            // Granted by item isn't compatible
            if(AURAS[aura].item) {
              return true
            }
            aurasSelected++
            // Too many auras selected
            if(aurasSelected > 1) {
              return true
            }
            // I think auls doesn't support heralds
            if(name == 'AULS_UPRISING' && AURAS[aura].buff) {
              return true
            }
          }
        }
      }

      // Disable essence worm if anything else is selected
      for (let item in LOCAL_ITEMS) {
        if($rootScope.itemGroup[item][index] && name == 'ESSENCE_WORM') {
          return true
        }
      }
      return false
    }

    // Disables most gems when something special is selected
    $rootScope.specialGemDisabled = function(name, index) {
      // Disable everything while viewing, or number fields
      if($rootScope.viewing) {
        return true
      }

      var ESSENCE = $rootScope.itemGroup['ESSENCE_WORM'][index]
      var AULS = $rootScope.itemGroup['AULS_UPRISING'][index]

      // Never disable anything that is selected, or if the specials aren't selected
      if($rootScope.auraGroup[name][index] || (!ESSENCE && !AULS)) {
        return false
      }

      // Disable if number field and essence worm is selected (and not flat)
      if(AURAS[name].number && !AURAS[name].flat && (ESSENCE || AULS)) {
        return true
      }

      // Disable if granted from item (incompatible with specials)
      if(AURAS[name].item && (ESSENCE || AULS)) {
        return true
      }

      // Disable buffs if auls is selected
      if(AURAS[name].buff && AULS) {
        return true
      }

      for (let aura in AURAS) {
        if($rootScope.auraGroup[aura][index]) {
          return true
        }
      }
      return false
    }

    // Tries to disable columns on incompatible items
    $rootScope.itemDisabled = function(name, index) {
      var isItemGroup = typeof index !== "undefined"
      var type = isItemGroup ? LOCAL_ITEMS[name]["type"] : $rootScope.ALL_ITEMS[name]["type"]

      // Disable everything while viewing
      if($rootScope.viewing) {
        return true
      }

      // Doesn't have any rules for disabling
      if(typeof type == "undefined") {
        return false
      }

      // Never disable anything that is selected
      if(isItemGroup && $rootScope.itemGroup[name][index]) {
        return false
      }
      if(!isItemGroup && $rootScope.settings[name]) {
        return false
      }

      // Allow multiple of the same item for separate itemgroups
      if(isItemGroup && globalTmp['itemComboUsed'][name]) {
        return false
      }

      // Disallow multipule item types to be selected at once
      if(isItemGroup) {
        for (let item in LOCAL_ITEMS) {
          if($rootScope.itemGroup[item][index] && LOCAL_ITEMS[item].type) {
            return true
          }
        }
      }

      // Hands are all together
      if(type == '1HAND' || type == 'WAND' || type == 'SHIELD') {
        return $rootScope.itemCombo['1HAND'] + $rootScope.itemCombo['WAND'] + $rootScope.itemCombo['SHIELD'] >= 2
      }

      if($rootScope.itemCombo[type] >= 1) {
        return true
      }

      return false
    }

    $rootScope.recalc = function(obj, skipHash) {
      // Toggle dependent settings
      if(typeof obj !== "undefined") {
        switch(obj.key) {
          case 'BLOOD_MAGIC':
            $rootScope.settings['MORTAL_CONVICTION'] = $rootScope.settings['BLOOD_MAGIC']
            break
          case 'ICHIMONJI':
            $rootScope.settings['ICHIMONJI2'] = false
            break
          case 'MIDNIGHT_BARGAIN':
            $rootScope.settings['MIDNIGHT_BARGAIN2'] = false
            break
        }
      }

      var globalReducedMana = $rootScope.settings['reducedMana'] || 0
      var globalLessMana = []
      var percentageReserved = [0, 0]
      var flatReserved = [0, 0]
      $rootScope.bloodMagic = false
      $rootScope.itemCombo = { "HELM": 0, "1HAND": 0, "WAND": 0, "BOOTS": 0, "CHEST": 0, "AMULET": 0, "SHIELD": 0, "RING": 0 }
      $rootScope.auraSelected = {}
      $rootScope.totalAuras = 0
      globalTmp = { itemComboUsed: {} }

      // Set simple modifiers
      for (let itemGroup in ITEMS) {
        for (let item in ITEMS[itemGroup]) {
          if($rootScope.settings[item] == true && ITEMS[itemGroup][item].special != true) {
            if(typeof ITEMS[itemGroup][item].reduced !== "undefined") {
              globalReducedMana += ITEMS[itemGroup][item].reduced
            }
            if(typeof ITEMS[itemGroup][item].less !== "undefined") {
              globalLessMana.push(ITEMS[itemGroup][item].less)
            }
            if(ITEMS[itemGroup][item].bloodMagic && typeof ITEMS[itemGroup][item].setTo == "undefined") {
              $rootScope.bloodMagic = true
            }
          }

          if($rootScope.settings[item] == true && ITEMS[itemGroup][item].type) {
            $rootScope.itemCombo[ITEMS[itemGroup][item].type]++
          }
        }
      }

      // Loop through again to pick up any modifiers affected by blood magic or reduced mana
      for (let itemGroup in ITEMS) {
        for (let item in ITEMS[itemGroup]) {
          if($rootScope.settings[item] == true && ITEMS[itemGroup][item].special != true) {
            if(typeof ITEMS[itemGroup][item].setTo != "undefined") {
              percentageReserved[ITEMS[itemGroup][item].bloodMagic ? 1 : 0] += ITEMS[itemGroup][item].setTo
            }
          }
        }
      }

      // Essence worm increases global mana reservation
      for (let i in $rootScope.auraGroups) {
        if($rootScope.itemGroup['ESSENCE_WORM'][i]) {
          globalReducedMana -= 40
        }
      }

      // Calculate auras from aura groups
      for (let i in $rootScope.auraGroups) {
        var localReducedMana = ($rootScope.auraGroup['REDUCED_MANA'][i] || 0) + globalReducedMana
        var localMutliplier = $rootScope.auraGroup['MULTIPLIER'][i] || 100
        $rootScope.bmGroup[i] = $rootScope.bloodMagic

        for (let localItem in LOCAL_ITEMS) {
          if(LOCAL_ITEMS[localItem].number == true) {
            if(typeof LOCAL_ITEMS[localItem].multi !== "undefined") {
              let multiplier = LOCAL_ITEMS[localItem].multi[parseInt($rootScope.itemGroup[localItem][i]) || 0]
              $rootScope.multiplier[localItem][i] = multiplier
              localMutliplier *= multiplier / 100
            }
            if(parseInt($rootScope.itemGroup[localItem][i]) && LOCAL_ITEMS[localItem].bloodMagic) {
              $rootScope.bmGroup[i] = true
            }
          }
          else if($rootScope.itemGroup[localItem][i] == true && LOCAL_ITEMS[localItem].special != true) {
            if(typeof LOCAL_ITEMS[localItem].multi !== "undefined") {
              localMutliplier *= LOCAL_ITEMS[localItem].multi / 100
            }
            else if(typeof LOCAL_ITEMS[localItem].reduced !== "undefined") {
              localReducedMana += LOCAL_ITEMS[localItem].reduced
            }
            if(LOCAL_ITEMS[localItem].bloodMagic) {
              $rootScope.bmGroup[i] = true
            }
          }

          // Track combos of items used
          if($rootScope.itemGroup[localItem][i] && LOCAL_ITEMS[localItem].type && !globalTmp['itemComboUsed'][localItem]) {
            globalTmp['itemComboUsed'][localItem] = true
            $rootScope.itemCombo[LOCAL_ITEMS[localItem].type]++
          }
        }

        for (let aura in AURAS) {
          var auraReserved = 0

          // Essence worm and auls uprising makes auras free
          if($rootScope.itemGroup['ESSENCE_WORM'][i] || $rootScope.itemGroup['AULS_UPRISING'][i]) {
            $rootScope.reserved[aura][i] = 0
          }
          // Aura has custom function
          else if(AURAS[aura].override) {
            if(AURAS[aura].number == true) {
              if(typeof $rootScope.reserved['NEXT_' + aura] == "undefined") {
                $rootScope.reserved['NEXT_' + aura] = {}
              }
              $rootScope.reserved['NEXT_' + aura][i] = calculateAura(AURAS[aura].cost, localReducedMana, globalLessMana, localMutliplier)
            }

            let payload = {
              rootScope: $rootScope,
              auraGroup: i,
              localReducedMana: localReducedMana,
              globalLessMana: globalLessMana,
              localMutliplier: localMutliplier,
              aura: aura
            }
            auraReserved = AURAS[aura].override(payload)
            $rootScope.reserved[aura][i] = auraReserved
          }
          // Flat aura
          else if(AURAS[aura].flat) {
            let flatCost = AURAS[aura].flat[parseInt($rootScope.auraGroup[aura][i]) || 0]
            flatCost = calculateAura(flatCost, localReducedMana, globalLessMana, localMutliplier, true)
            $rootScope.reserved[aura][i] = flatCost
            flatReserved[$rootScope.bmGroup[i] ? 1 : 0] += flatCost
          }
          // Numerical aura
          else if(AURAS[aura].number == true) {
            if(typeof $rootScope.reserved['NEXT_' + aura] == "undefined") {
              $rootScope.reserved['NEXT_' + aura] = {}
            }
            $rootScope.reserved['NEXT_' + aura][i] = calculateAura(AURAS[aura].cost, localReducedMana, globalLessMana, localMutliplier)
            let number = parseInt($rootScope.auraGroup[aura][i]) || 0
            auraReserved = calculateAura(AURAS[aura].cost, localReducedMana, globalLessMana, localMutliplier) * number
            $rootScope.reserved[aura][i] = auraReserved
          }
          // Standard checkbox aura
          else {
            auraReserved = calculateAura(AURAS[aura].cost, localReducedMana, globalLessMana, localMutliplier)
            $rootScope.reserved[aura][i] = auraReserved
          }

          // Apply if selected
          if($rootScope.auraGroup[aura][i]) {
            $rootScope.auraSelected[aura] = true
            percentageReserved[$rootScope.bmGroup[i] ? 1 : 0] += auraReserved
          }
        }
      }

      for (let aura in $rootScope.auraSelected) {
        if(AURAS[aura].aura) {
          $rootScope.totalAuras++
        }
      }

      $rootScope.settings['life'] = $rootScope.life
      $rootScope.settings['mana'] = $rootScope.mana

      $rootScope.life = Math.max(Math.min($rootScope.life, 30000), 1)
      $rootScope.mana = Math.max(Math.min($rootScope.mana, 30000), 1)

      $rootScope.lifeReservedNumeric = Math.ceil($rootScope.life * (percentageReserved[1] / 100)) + flatReserved[1]
      $rootScope.manaReservedNumeric = Math.ceil($rootScope.mana * (percentageReserved[0] / 100)) + flatReserved[0]

      /*
        This is a very special case: If you're running CI the game will not allow you
        to run any auras off life. I think GGG made a manual exception to always
        reserve something, even though they're rounding down all the time.

        I did the same thing to mana for consistency, although I don't think its
        possible to have 1 mana
      */
      if($rootScope.lifeReservedNumeric == 0 && $rootScope.life == 1 && percentageReserved[1]) {
        $rootScope.lifeReservedNumeric = 1
      }
      if($rootScope.manaReservedNumeric == 0 && $rootScope.mana == 1 && percentageReserved[0]) {
        $rootScope.manaReservedNumeric = 1
      }

      // Keep percentages within 0-100 range
      var percentFixed = function(num) {
        return Math.max(Math.round(num * 100), 0)
      }

      $rootScope.lifeReservedPercent = percentFixed($rootScope.lifeReservedNumeric / $rootScope.life)
      $rootScope.manaReservedPercent = percentFixed($rootScope.manaReservedNumeric / $rootScope.mana)

      // Global blood magic removes all mana
      if($rootScope.bloodMagic) {
        $rootScope.manaReservedNumeric = 0
        $rootScope.manaReservedPercent = 100
      }

      // Url encoding!
      if(!skipHash) {
        var bin = ""
        var hash = []

        // Encode settings
        for (let s in settingsEncode) {
          if(settingsEncode[s][1] == 1) {
            bin += $rootScope.settings[settingsEncode[s][0]] ? 1 : 0
          }
          else {
            bin += pad(($rootScope.settings[settingsEncode[s][0]] ? parseInt($rootScope.settings[settingsEncode[s][0]]) : 0).toString(2), settingsEncode[s][1])
          }
        }

        hash.push(alpha.encode(parseInt(bin,2)))

        // Encode each aura group
        for (let i in $rootScope.auraGroups) {
          var hashGroup = []
          for (let a in aurasEncode) {
            var bin = ""
            for (let b in aurasEncode[a]) {
              var value = null
              if(LOCAL_ITEMS[aurasEncode[a][b][0]]) {
                value = $rootScope.itemGroup[aurasEncode[a][b][0]][i]
              }
              else if(AURAS[aurasEncode[a][b][0]]) {
                value = $rootScope.auraGroup[aurasEncode[a][b][0]][i]
              }
              else if(typeof $rootScope.auraGroup[aurasEncode[a][b][0]][i] !== "undefined") {
                value = $rootScope.auraGroup[aurasEncode[a][b][0]][i]
              }
              else
              {
                console.log('Could not find:', aurasEncode[a][b][0])
              }

              if(aurasEncode[a][b][1] == 1) {
                bin += value ? 1 : 0
              }
              else {
                bin += pad((value ? parseInt(value) : 0).toString(2), aurasEncode[a][b][1])
              }
            }
            hashGroup.push(alpha.encode(parseInt(bin,2)))
          }
          hash.push(hashGroup.join("."))
        }

        location.replace("#" + hash.join("/"))
      }
    }

    var hash = window.location.hash.substr(1)

    // Load from URL
    if(hash.length > 0) {
      data = hash.split("/")
      var bin = pad(alpha.decode(data[0]).toString(2), 65)
      var pos = 0

      for(i=settingsEncode.length - 1; i >= 0; i--) {

        pos += settingsEncode[i][1]
        var bindata = parseInt(bin.substr((pos * -1 ? pos * -1 : 0), settingsEncode[i][1]).toString(), 2)

        if(settingsEncode[i][1] == 1) {
          bindata = bindata ? true : false
        }
        $rootScope.settings[settingsEncode[i][0]] = bindata
      }


      $rootScope.auraGroups = []
      for(let i=0; i < data.length - 1; i++) {
        $rootScope.auraGroups.push(i)
      }

      for(a=0; a < $rootScope.auraGroups.length; a++) {

        // I discovered I met the javascript limit of 2^53 for binary numbers really fast... so here's
        // a period seperator to space out some of the data in the urls, added later unfortunately.

        var auraData = data[a + 1].split(".")

        for(d=0; d <= auraData.length - 1; d++) {
          var bin = pad(alpha.decode(auraData[d]).toString(2), 65)
          var pos = 0

          for(i=aurasEncode[d].length - 1; i >= 0; i--) {
            pos += aurasEncode[d][i][1]
            var bindata = parseInt(bin.substr((pos * -1 ? pos * -1 : 0), aurasEncode[d][i][1]).toString(), 2)

            if(aurasEncode[d][i][1] == 1) {
              bindata = bindata ? true : false
            }

            if(LOCAL_ITEMS[aurasEncode[d][i][0]]) {
              $rootScope.itemGroup[aurasEncode[d][i][0]][a] = bindata
            }
            else {
              $rootScope.auraGroup[aurasEncode[d][i][0]][a] = bindata
            }
          }
        }
      }
      $rootScope.life = $rootScope.settings['life']
      $rootScope.mana = $rootScope.settings['mana']
      $rootScope.viewing = true
    }

    document.querySelectorAll("a[rel=external]").forEach(function(link) {
      link.target = "_blank"
    })

    // Finished loading, display app
    $rootScope.recalc({}, true)
    angular.element(document.getElementById("loading")).remove()
    document.getElementById("container").style.display = "block"
})
.directive("auraGroup", function() {
  return {
    restrict: 'E',
    template: `
<div class="col-sm-6 col-md-4 itemBlock" ng-class="{ edited: auraGroup[key][index] }">
  <label ng-class="{ selected: auraSelected[key], disabled: viewing || specialGemDisabled(key, index), dim: isDimmed(bmGroup[index], item.flat, key, index) }">
    <img ng-if="!item.singleImg" ng-src="img/aura/{{ key | lowercase }}.png">
    <img ng-if="!item.singleImg" ng-src="img/gem/{{ key | lowercase }}.png">
    <img ng-if="item.singleImg" ng-src="img/{{ key | lowercase }}.png">
    <input ng-if="item.number" type="number" ng-change="recalc(this)" ng-disabled="viewing || specialGemDisabled(key, index)" ng-model="auraGroup[key][index] "min="0" max="{{ item.max }}" ng-pattern="onlyNumbers" placeholder="num" />
    <input ng-if="!item.number" type="checkbox" ng-change="recalc(this)" ng-disabled="viewing || specialGemDisabled(key, index)" ng-model="auraGroup[key][index]" value="1" />
    <ng-container ng-if="!item.description">
      {{ item.title }}
    </ng-container>
    <abbr ng-if="item.description" title="{{ item.description }}">
      {{ item.title }}
    </abbr>
    <span class="reserved">{{ reserved[key][index] }}<span ng-if="item.cost!==undefined">%</span></span>
    <div class="cr"></div>
  </label>
</div>`
  }
})
.directive("itemGroup", function() {
  return {
    restrict: 'E',
    template: `
<div class="col-sm-6 col-md-4 itemBlock" ng-class="{ edited: itemGroup[key][index], bm: item.bloodMagic }">
  <label ng-class="{ disabled: itemDisabled(key, index) || specialDisabled(key, index) }">
    <img ng-src="img/{{ key | lowercase | remove_numbers }}.png">
    <input ng-if="item.number" type="number" ng-change="recalc(this)" ng-disabled="itemDisabled(key, index) || specialDisabled(key, index)" ng-model="itemGroup[key][index]" min="0" max="{{ item.max }}" ng-pattern="onlyNumbers" placeholder="num" />
    <input ng-if="!item.number" type="checkbox" ng-change="recalc(this)" ng-disabled="itemDisabled(key, index) || specialDisabled(key, index)" ng-model="itemGroup[key][index]" value="1" />
    <ng-container ng-if="!item.description">
      {{ item.title }}
    </ng-container>
    <abbr ng-if="item.description" title="{{ item.description }}">
      {{ item.title }}
    </abbr>
    <span class="reserved" ng-if="item.number==true">
      <span ng-if="item.multi!==undefined">x{{ multiplier[key][index] }}%</span>
    </span>
    <span class="reserved" ng-if="item.number!==true">
      <span ng-if="item.multi!==undefined">x{{ item.multi }}%<span ng-if="item.special">*</span></span>
      <span ng-if="item.less!==undefined">{{ item.less }}% less<span ng-if="item.special">*</span></span>
      <span ng-if="item.reduced!==undefined">x{{ 100 - item.reduced }}%<span ng-if="item.special">*</span></span>
      <span ng-if="item.setTo!==undefined">{{ item.setTo }}%<span ng-if="item.special">*</span></span>
    </span>
    <div class="cr"></div>
  </label>
</div>`
  }
})
.directive("setting", function() {
  return {
    restrict: 'E',
    template: `
<div class="col-sm-6 col-md-4 itemBlock" ng-class="{ edited: settings[key], bm: item.bloodMagic }">
  <label ng-class="{ disabled: {{ item.disabled || false }} || itemDisabled(key) }">
    <img ng-src="img/{{ key | lowercase | remove_numbers }}.png">
    <input type="checkbox" ng-disabled="{{ item.disabled || false }} || itemDisabled(key)" ng-change="recalc(this)" ng-model="settings[key]" value="1" />
    <ng-container ng-if="!item.description">
      {{ item.title }}
    </ng-container>
    <abbr ng-if="item.description" title="{{ item.description }}">
      {{ item.title }}
    </abbr>
    <span class="reserved">
      <span ng-if="item.multi!==undefined">x{{ item.multi }}%<span ng-if="item.special">*</span></span>
      <span ng-if="item.less!==undefined">{{ item.less }}% less<span ng-if="item.special">*</span></span>
      <span ng-if="item.reduced!==undefined">x{{ 100 - item.reduced }}%<span ng-if="item.special">*</span></span>
      <span ng-if="item.setTo!==undefined">{{ item.setTo }}%<span ng-if="item.special">*</span></span>
    </span>
    <div class="cr"></div>
  </label>
</div>`
  }
})
.filter('remove_numbers', function() {
  return function(input) { return input.replace(/[0-9]/g, ''); }
})
