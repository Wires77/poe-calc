Path of Exile Aura Calculator
========

This is a reserved mana calculator for the PC game Path of Exile (http://www.pathofexile.com). Me and a friend of mine enjoy the game but had difficulty predicting what our reserved mana would be with complicated aura setups (varying gem levels, support gems attached to auras, partial blood magic, etc), so I made this calculator. I don't have plans for any major feature additions to this calculator other than some quality of life updates and keeping it up to date with the latest balance changes in Path of Exile, I like to keep things simple and believe in doing one thing and doing it well.

Some of the features:

* Calculates multiple groups of auras, separated from each other
* Mobile support so you can calculate auras while skydiving for the most extreme of users
* It's linkable with short URLs, example: https://poe.mikelat.com/#mcL33t/UZghshj/fnQh_xb
* Said links above will display in a simplified "summary mode" upon loading a url, which simplies the UI to one where you can just see selected auras and important information. Editing the setup is still permitted by clicking a button at the top.
* Displays life/mana predictions in bubbles similar to that of the UI of the game
* Flexibility with regards to various support gems and a custom input for power users
* The calculator itself is just a simple HTML/JS/CSS application with no backend database or dynamic content at all.

The images used are owned by GGG and I take no credit for them. This calculator is licensed under MIT, so you're welcome to edit, merge it with your own project, fork, mirror or whatever to it if for some reason in the future I'm not updating my version which is hosted at http://poe.mikelat.com

I've developed and tested this in firefox and chrome. I have no plans to support backwards compatibility in the calculator, it is meant to show the most up to date information, even if it becomes impossible in a future patch.
